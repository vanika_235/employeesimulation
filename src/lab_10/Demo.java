/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lab_10;

import java.util.Date;
public class Demo {
    public static void main (String[] args){
        Employee employee= new Employee ("2345", "jack", new Date (1992, 04, 15, 3, 23,8));
        boolean thisYearPromotionValid= employee.isPromotionDueThisYear();
        IncomeTaxCalculator incomeTaxCalculator= new IncomeTaxCalculator();
        double currentYearIncomeTax= incomeTaxCalculator.calcIncomeTaxForCurrentYear(employee);
        System.out.println(employee +"this year promotion valid" + thisYearPromotionValid+"income tax: $" +currentYearIncomeTax);
}
}